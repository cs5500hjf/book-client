import books from './books';
import users from './users';
import currentuser from './current-user';
import collections from './collections';

import { combineReducers } from 'redux';

const allReducers = combineReducers({
    books,
    users,
    currentuser,
    collections
});

export default allReducers;
