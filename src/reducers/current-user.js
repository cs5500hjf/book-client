export default (state = null, action) => {
    
    switch (action.type) {
      case 'SIGNED_IN':
        return action.payload
      case 'SIGNED_OUT':
        return null
      default:
        return state
    }
  }