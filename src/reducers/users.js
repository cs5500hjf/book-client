export default (state=null, action) => {

    switch(action.type) {
        case "USERS_FETCH":
            return action.payload;
        default: return state;
    }
}