export default (state=null, action) => {

    switch(action.type) {
        case "BOOKS_FETCH":
            return action.payload;
        default: return state;
    }
}