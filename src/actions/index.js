export function getUsers() {
    return (dispatch) => {
        let url = `https://msd-team-1.herokuapp.com/users`;
        dispatch( {
            type: "USERS_FETCH_START"
        });
        return  fetch(url)
                .then(res => res.json())
                .then(users => {
                    dispatch({
                        type: 'USERS_FETCH',
                        payload: users
                    });
                    return {
                        type: 'USERS_FETCH',
                        payload: users
                    }
                });
    }
};

export function getBooks() {
    return (dispatch) => {
        let url = `https://msd-team-1.herokuapp.com/books`;
        dispatch( {
            type: "BOOKS_FETCH_START"
        });
        return  fetch(url)
                .then(res => res.json())
                .then(books => {
                    dispatch({
                        type: 'BOOKS_FETCH',
                        payload: books
                    });
                    return {
                        type: 'BOOKS_FETCH',
                        payload: books
                    }
                });
    }
};

export function setCurrentUser(user) {
    return (dispatch) => {
        dispatch({
            type: 'SIGNED_IN',
            payload: user
        });
    }
};

export function resetCurrentUser() {
    return (dispatch) => {
        dispatch({
            type: 'SIGNED_OUT'
        });
    }
};

export function getCollection() {

    const currentuser = JSON.parse(localStorage.getItem('currentuser'));

    let url = `https://msd-team-1.herokuapp.com/users/${currentuser.id}/collections`;

    return (dispatch) => {
        dispatch({
            type: 'COLLECTIONS_FETCH_START'
        });
        return fetch(url)
                .then(res => res.json())
                .then(collections => {
                    console.log(collections);
                   
                    dispatch({
                        type: 'COLLECTIONS_FETCH',
                        payload: collections
                    });
                    return {
                        type: 'COLLECTIONS_FETCH',
                        payload: collections
                    };
                });
    }
}
