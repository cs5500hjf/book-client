import React, { Component } from "react";

class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: ''
    }
    this.handleChange = this.handleChange.bind(this);
    this.register = this.register.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    // console.log('Val:' + value + ' ' + name);
    this.setState({
      [name]: value,
      error: ''
    });
  }

  register() {
    // console.log(this.state);
    if(!this.state.name || !this.state.username || !this.state.password || !this.state.confirm || !this.state.email) {
      this.setState({
        error: 'Complete all Mandatory fields'
      });
    } else if (this.state.password !== this.state.confirm) {
      this.setState({
        error: 'Passwords dont match'
      });
    } else {
      const user = {
        name: this.state.name,
        password: this.state.password,
        email: this.state.email,
        user_name: this.state.username
      };
      // console.log(user);
      let url = `https://msd-team-1.herokuapp.com/users`;
      fetch(url, {
        method:'POST',
            body: JSON.stringify(user),
            headers: {
                'Content-Type':'application/json'
            }
      })
      .then((res) => {
        // console.log(res);
        if(res.status === 200) {
          alert('User registered successfully. Please Login.');
          this.props.toggleRegister();
        } else if(res.status === 409) {
          this.setState({
            error: 'Username already exists'
          });
        } else {
          this.setState({
            error: res.json()
          });
        }
      })
    }
  }

  render() {
    return (
      <section>
        <div className="container-fluid ">
            <hr className="my-5"/>
            <div className="container">
                <div className="row mt-5 pt-3 justify-content-center">

                    <div className="col-lg-6 col-6 mt-1 mx-lg-4">

                        <section className="section extra-margins pb-3 text-center text-lg-left">

                            <div className="row mb-5 pb-3">

                                <div className="col-md-12">
                                <section className="form-simple">

                                  <div className="card">

                                      <div className="header pt-3 grey lighten-2">

                                          <div className="row d-flex justify-content-start">
                                              <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5">Register</h3>
                                          </div>

                                      </div>

                                      <div className="card-body mx-4 mt-4">

                                          <div className="md-form">
                                              <input type="text" id="Form-name" name="name" className="form-control"  onChange={this.handleChange} />
                                              <label htmlFor="Form-name">Name</label>
                                          </div>

                                          <div className="md-form">
                                              <input type="text" id="Form-email" name="email" className="form-control"  onChange={this.handleChange} />
                                              <label htmlFor="Form-email">Email</label>
                                          </div>

                                          <div className="md-form">
                                              <input type="text" id="Form-username" name="username" className="form-control"  onChange={this.handleChange} />
                                              <label htmlFor="Form-username">Username</label>
                                          </div>

                                          <div className="md-form pb-3">
                                              <input type="password" id="Form-password" name="password" className="form-control"  onChange={this.handleChange}/>
                                              <label htmlFor="Form-password">Password</label>
                                              
                                          </div>

                                          <div className="md-form pb-3">
                                              <input type="password" id="Form-confirm2" name="confirm" className="form-control"  onChange={this.handleChange}/>
                                              <label htmlFor="Form-confirm2">Confirm Password</label>
                                              <div className="font-small red-text d-flex justify-content-start">{this.state.error}</div>
                                          </div>
                                         

                                          <div className="text-center mb-4">
                                              <button type="button" className="btn btn-danger  z-depth-2" onClick={this.register}>Register</button>
                                              <button type="button" className="btn btn-grey  z-depth-2" onClick={this.props.toggleRegister}>Cancel</button>
                                          </div>
                                          
                                      </div>

                                  </div>

                                  </section>

                                </div>

                            </div>

                          </section>

                      </div>

                  </div>

              </div>

          </div>

        </section>
    );
  }
}

export default Register;
