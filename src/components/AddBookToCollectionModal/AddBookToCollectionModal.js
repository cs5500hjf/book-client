import React, { Component } from "react";

class AddBookToCollectionModal extends Component {

  constructor(props) {
    super(props);
    this.addFavoriteBook = this.addFavoriteBook.bind(this);
  }

  addFavoriteBook(book){
    const currentuser = JSON.parse(localStorage.getItem('currentuser'));

    let url = `https://msd-team-1.herokuapp.com/users/${currentuser.id}/collections/${this.props.collection_name}/books/${book.id}`;
    fetch(url, {
      method:'POST',
      headers: {
        'Content-Type':'application/json'
      }

    })
      .then(res => res.json())
      .then(data => {
        alert(data);
        this.props.reloadList();
      });

  }


  renderBooksModal(books, favbooks){

    if (books) {
      books = books.sort((a, b) => {
        if(a.id < b.id) return -1;
        if(a.id > b.id) return 1;
        return 0;
      });
      const trows =  books.map(book =>
        <tr className="clickable-row" key={book.id}>
          <td>{book.id}</td>
          <td>{book.name}</td>
          <td>{book.author}</td>
          <td>{book.subject}</td>

          <td>
            <button disabled={favbooks.some(e => e.id === book.id)} onClick={() => {this.addFavoriteBook(book)}}
                    className="btn btn-sm btn-danger">{favbooks.some(e => e.id === book.id)? "Added ": "Add Book"}</button>
          </td>
        </tr>
      );
      return trows;
    }


  }

  render() {
    return (
      <div className="modal fade" id="addBookToCollec" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div className="modal-dialog modal-lg" role="document">

          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title w-100" id="myModalLabel">Add to Collection</h4>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {this.props.collection_name ?
                <div className="table-wrapper table-responsive">
                  <table className="table table-hover mb-0">
                    <thead>
                    <tr>
                      <th className="th-lg">
                        Book ID
                      </th>
                      <th className="th-lg">
                        Book Name
                      </th>
                      <th className="th-lg">
                        Author
                      </th>
                      <th className="th-lg">
                        Subject
                      </th>
                      <th className="th-lg">
                        ACTION
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      this.renderBooksModal(this.props.books, this.props.favbooks)
                    }
                    </tbody>
                  </table>
                </div>
                : null}

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddBookToCollectionModal;
