import React, { Component } from "react";

class UpdateNoteModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      note: null,
      error: '',
      value:''
    }

    this.modalref = React.createRef();
    this.closeModal = this.closeModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getNote = this.getNote.bind(this);
    
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    // const name = target.name;
    // console.log('Val:' + value + ' ' + name);
    this.setState({
      value: value,
      note: {
        id: this.state.note.id,
        book_id: this.state.note.book_id,
        note: value
      }
    });
    // console.log(this.state);
  }

  closeModal() {
    this.modalref.current.click();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.note && nextProps.note !== this.props.note) {
      console.log(nextProps)
      this.setState({
        value: nextProps.note.note,
        note: nextProps.note
      });
      
    }
  }

  getNote() {
    if (this.props.note && this.props.note !== null) {
      console.log(this.props.note.note);
      return this.props.note.note;
    } else {
      return 'hello';
    }
  }

  updateNote() {
    if (this.props.note.note === this.state.value) {
      this.setState({
        error: 'Note is same as previous value. Please update'
      });
    } else {
      this.props.updateNote(this.state.note, this.closeModal);
    }
  }

  render() {

    return (
      <div className="modal fade" id="updateNoteModal" tabIndex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">

        <div className="modal-dialog modal-lg" role="document">

          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title w-100" id="myModalLabel">Add Note</h4>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" ref={this.modalref}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <section className="form-simple ">
                { this.state.value === '' ? null : 
                  <div className="md-form pb-3">
                    <textarea id="Form-note" name="note" className="md-textarea form-control" defaultValue={this.state.value} onChange={this.handleChange}/>
                    <label className="active" htmlFor="Form-note">Note</label>
                    <div className="font-small red-text d-flex justify-content-start">{this.state.error}</div>
                  </div>
                }
               
              </section>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
              <button type="button" disabled={!this.state.note || this.state.note === null} className="btn btn-primary btn-sm" onClick={() => {this.updateNote()}}>Submit</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UpdateNoteModal;
