import React, { Component } from "react";
import {getBooks, getCollection} from "../../actions";
import {connect} from "react-redux";


class FavoritesManage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value : {
        favoritesname: "",
      }
    }
    this.handleChange = this.handleChange.bind(this);

    this.addNewCollection = this.addNewCollection.bind(this);

  }

  handleChange(e) {
    const newVal = this.state.value;

    newVal[e.target.id] = e.target.value;
    this.setState({ value: newVal });
  }

  componentDidMount(){
    this.props.getBooks();
    this.props.getCollection();
    // console.log(this.props.books);
  }


  addNewCollection() {
    let currentuser = JSON.parse(localStorage.getItem('currentuser'));
    let url = `https://msd-team-1.herokuapp.com/users/${currentuser.id}/collections`;

    let info = {};
    info.collection_name = this.state.value.favoritesname;

    fetch(url, {
      method:'POST',
      body: JSON.stringify(info),
      headers: {
        'Content-Type':'application/json'
      }
    })
      .then(res => res)
      .then((res) => {
        var data = res;
        console.log(data);
        this.props.getCollection();
        this.props.history.push('/home/library');
      })

    // todo: link to save book to server
    // console.log(this.state);
  }

  render() {
    // console.log(this.props.books);
    return (
      <main className="pt-5 mx-lg-3">
        <div className="container-fluid mt-5">
          <div className="container ">
          <div className="row mt-5 pt-3 justify-content-center">
            <div className="card card-cascade narrower col-lg-6 col-6 mt-1 mx-lg-4">
            <div className="px-4"><br/>
                  <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5 text-center">Add a new collection</h3><br/>
                <section className="form-simple">
                  <div className="md-form pb-3">
                    <label htmlFor="favoritesname">Add Collection Name</label>
                    <input className="form-control" id="favoritesname" type="text" onChange={this.handleChange}/>
                  </div>
                  <div className="text-center mb-4">
                    <button disabled={!this.state.value.favoritesname} className="btn btn-danger" onClick= {() => {this.addNewCollection()}} >Add</button>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
        </div>
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {
    books: state.books,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getBooks : () => {
      dispatch(getBooks());
    },
    getCollection : () => {
      dispatch(getCollection());
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesManage);


// export default FavoritesManage;
