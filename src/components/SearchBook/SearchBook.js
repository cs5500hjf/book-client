import React, { Component } from "react";

class SearchBook extends Component {

  constructor(props) {
    super(props);
    this.state = {
      books: null,
      author: '',
      subject: '',
      start: '',
      end: '',
      error: ''
    }

    this.getList = this.getList.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    // console.log('Val:' + value + ' ' + name);
    this.setState({
      [name]: value
    });
  }

  getList(books) {

    if (books) {
      books = books.sort((a, b) => {
        if(a.id < b.id) return -1;
        if(a.id > b.id) return 1;
        return 0;
      });
      const trows =  books.map(book =>
        <tr key={book.id}>
          <td>{book.id}</td>
          <td>{book.name}</td>
          <td>{book.author}</td>
          <td>{book.subject}</td>
          <td>{book.publish_date}</td>
        </tr>
      );
      return trows;
    }
  }

  submitHandler(e) {
    e.preventDefault();
    console.log(this.state)
    let searchData = {};
    let flag = false;
    if (this.state.author !== null && this.state.author !== '') {
      searchData.author = this.state.author
      flag = true;
    }
    if (this.state.subject !== null && this.state.subject !== '') {
      searchData.subject = this.state.subject
      flag = true;
    }
    if (this.state.start !== null && this.state.start !== '' && this.state.end !== null && this.state.end !== '') {
      searchData.time_period_start = this.state.start
      searchData.time_period_end = this.state.end
      flag = true;
    }

    // console.log(flag)

    if (flag) {
      let url = `https://msd-team-1.herokuapp.com/search`
      fetch(url, {
        method:'POST',
            body: JSON.stringify(searchData),
            headers: {
                'Content-Type':'application/json'
            }
      })
      .then(res => res.json())
      .then((data) => {
        this.setState({
          error: '',
          books: data
        })
        // console.log(data);
      })
    } else {
      this.setState({
        error: 'Please select atleast one criteria'
      })
    }
  }

  render() {
    return (
      <main className="pt-5 mx-lg-5">
        <div className="container-fluid mt-5">
          <div  className="card card-cascade narrower">
              <div className="px-4">
                  <br/>
                  <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5 text-center"> Search Book </h3><hr/><br/>
                  {/* <section className="form-simple ">

                    <div className="md-form pb-3">
                        <input type="text" id="Form-userid" disabled name="id" className="form-control" />
                        <label className="active" htmlFor="Form-userid">User Id</label>
                    </div>
                  </section> */}
                  <form>
                      
                      <div className="form-row"> 
                          <div className="col">                      
                              <div className="md-form">
                                <input type="text" id="author"  name="author" className="form-control"  onChange={this.handleChange} />
                                <label className="active" htmlFor="author">Author</label>
                              </div>
                          </div>
                          <div className="col">                      
                              <div className="md-form">
                                <input type="text" id="subject"  name="subject" className="form-control"  onChange={this.handleChange} />
                                <label className="active" htmlFor="subject">Subject</label>
                              </div>
                          </div>  
                          <div className="col">                      
                              <div className="md-form">
                                <input type="date" id="start"  name="start" className="form-control"  onChange={this.handleChange} />
                                <label className="active" htmlFor="start">Start date</label>
                              </div>
                          </div>  
                          <div className="col">                      
                              <div className="md-form">
                                <input type="date" id="end"  name="end" className="form-control"  onChange={this.handleChange} />
                                <label className="active" htmlFor="end">End date</label>
                              </div>
                          </div>                                                  
                          
                      </div>
                      <div className="form-row">
                        <div className="font-small red-text d-flex justify-content-start">{this.state.error}</div>
                      </div>
                      <div className="form-row">
                        <button className="btn btn-sm btn-primary" onClick={this.submitHandler}> Search </button>
                      </div>
                      
                  </form>
                  <br/>
                  <div hidden={!(this.state.books && this.state.books.length > 0)} className="table-wrapper table-responsive">
                      <table className="table table-hover mb-0">
                          <thead>
                              <tr>
                                  <th className="th-lg">
                                      Book ID
                                  </th>
                                  <th className="th-lg">
                                      Book Name
                                  </th>
                                  <th className="th-lg">
                                      Author
                                  </th>
                                  <th className="th-lg">
                                      Subject
                                  </th>
                                  <th className="th-lg">
                                      Published Date
                                  </th>
                                  
                              </tr>
                          </thead>

                          <tbody>
                            {
                                this.getList(this.state.books)
                            }
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>        
        </div>   
      </main>
    );
  }
}

export default SearchBook;
