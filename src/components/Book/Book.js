import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import Notes from '../Notes/';
import { connect } from 'react-redux';
import { getBooks } from '../../actions/index';
import ModalPrompt from '../ModalPrompt/';

class Book extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      editing: false, 
      enableNotes: false,
      book: this.props.book
    };
    this.handleChange = this.handleChange.bind(this);
    this.updateBook = this.updateBook.bind(this);
  }

  render() {
    if (this.props.book) {
      console.log('sf')
      return this.renderBook(this.props.book);
    } else {
      return <div />;
    }
  }

  viewNotes(book) {
    this.setState({
      enableNotes: true
    })
  }

  updateBook() {
    let url = `https://msd-team-1.herokuapp.com/books/` + this.state.book.id;

    var info = {};
    info.name = this.state.book.name;
    info.author = this.state.book.author;
    info.publish_date = this.state.book.publish_date;
    info.subject = this.state.book.subject;
    // console.log(info);
    


    fetch(url, {
        method:'PUT',
        body: JSON.stringify(info),
        headers: {
            'Content-Type':'application/json'
        }
    })
    .then((json) => {
        console.log('book updated');
    })
    this.setState({
      editing: false
    })
    // console.log('doesnt work yet');

    
}
deleteBook() {
  let url = `https://msd-team-1.herokuapp.com/books/` + this.state.book.id;

  console.log(this.state.book);
  


  fetch(url, {
      method:'DELETE',
      headers: {
          'Content-Type':'application/json'
      }
  })
  .then(res => res.json())
  .then((json) => {
      console.log('book deleted');
      this.props.getBooks();
  })
  this.setState({
    editing: false
  })
  this.props.returnToLib();

  
}

handleChange(e) {
  var newVal = this.state.book;
  newVal[e.target.id] = e.target.value;
  this.setState({ value: newVal });
}

  renderBook(book) {
    return <div className="container">
      <div className="row mt-5 pt-3 justify-content-center">
        <div className="card card-cascade narrower col-lg-9 col-9 mt-1 mx-lg-4">
          { !this.state.enableNotes ?
          <div  className="px-4">
            <br/>
            <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5 text-center"> BOOK DETAILS </h3><hr/><br/>
            <section className="form-simple ">
              <div className="md-form pb-3">
                  <input type="text" id="id" disabled name="id" className="form-control" value={book.id} onChange={this.handleChange} />
                  <label className="active" htmlFor="Form-bookid">Book ID</label>
              </div>

              <div className="md-form pb-3">
                  <input type="text" id="name" disabled={!this.state.editing} name="name" className="form-control" defaultValue={book.name} onChange={this.handleChange}/>
                  <label className="active" htmlFor="Form-bookname">Book Name</label>
              </div>

              <div className="md-form pb-3">
                  <input type="text" id="author" disabled={!this.state.editing} name="author" className="form-control" defaultValue={book.author} onChange={this.handleChange} />
                  <label className="active" htmlFor="Form-bookauthor">Author</label>
              </div>

              <div className="md-form pb-3">
                  <input type="text" id="subject" disabled={!this.state.editing} name="genre" className="form-control" defaultValue={book.subject} onChange={this.handleChange}/>
                  <label className="active" htmlFor="Form-bookgenre">Genre</label>
              </div>

              <div className="md-form pb-3">
                  <input type="text" id="publish_date" disabled={!this.state.editing} name="pusblisheddate" className="form-control" defaultValue={book.publish_date} onChange={this.handleChange}/>
                  <label className="active" htmlFor="Form-bookpusblisheddate">Published Date</label>
              </div>

              <div className="text-center mb-4">
                { this.state.editing ?
                  <div><button type="button"  className="btn btn-warning z-depth-2" onClick={() => this.updateBook()}>Save</button>
                  <button type="button" className="btn btn-grey z-depth-2" onClick={() => { this.setState({ editing: false})}}>Cancel</button></div> :
                  <div>
                    <button type="button" className="btn btn-info z-depth-2" onClick={() => {this.viewNotes(book)}}>View Notes</button>
                    <button type="button" className="btn btn-warning z-depth-2" onClick={() => { this.setState({ editing: true})}}>Update</button>
                    <button type="button" className="btn btn-danger z-depth-2" data-toggle="modal" data-target="#promptModalSm" >Delete</button>
                    <button type="button" className="btn btn-grey z-depth-2" onClick={this.props.bookCallback}>Back</button>
                  </div>
                }
              </div>
            </section>
          </div> :
          <Notes book={book} hidden={!this.state.enableNotes} goBack={() => {this.setState({enableNotes: false})}} /> }
          <ModalPrompt title="Delete Book" body="Are you sure you want to delete this book ?" cb={(closeModal) => {this.deleteBook(); closeModal();}} />
        </div>
      </div>
    </div>;
  }
}

function mapStateToProps(state) {
  return {
      books: state.books
  }
}

function mapDispatchToProps(dispatch) {
  return {
      getBooks : () => {
          dispatch(getBooks());
      } 
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Book));
