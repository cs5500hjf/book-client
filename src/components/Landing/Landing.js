import React, { Component } from "react";
import Login from '../Login/';
import Register from '../Register/';

class Landing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      register: false
    }
    this.toggleRegister = this.toggleRegister.bind(this);
  }

  render() {
    return (
        !this.state.register ? <Login toggleRegister={this.toggleRegister}/> : <Register toggleRegister={this.toggleRegister}/> 
    );
  }

  toggleRegister() {
    this.setState({
      register: !this.state.register
    })
  }
}

export default Landing;
