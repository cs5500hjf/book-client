import React, { Component } from "react";

class ModalPrompt extends Component {

  constructor(props) {
    super(props);

    this.submitHandler = this.submitHandler.bind(this);
    this.modalref = React.createRef();
    this.closeModal = this.closeModal.bind(this);
  }

  submitHandler() {
    console.log('yes')
    this.props.cb(this.closeModal);
  }

  closeModal() {
    this.modalref.current.click();
  }

  render() {
    return (
      <div className="modal fade" id="promptModalSm" tabIndex="-1" role="dialog" aria-labelledby="promptModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title w-100" id="myModalLabel">{this.props.title ? this.props.title : ''}</h4>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" ref={this.modalref}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p> {this.props.body ? this.props.body : ''}</p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary btn-sm" data-dismiss="modal">No</button>
              <button type="button" className="btn btn-primary btn-sm" onClick={() => {this.submitHandler()}}>Yes</button>
            </div>
          </div>
        </div>
      </div>);
  }
}

export default ModalPrompt;
