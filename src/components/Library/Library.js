import React, { Component } from "react";
import { connect } from 'react-redux';
import { getBooks, getUsers } from '../../actions/index';
import Book from '../Book/';
import LoanModal from '../LoanModal/';

class Library extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      viewBook: false,
      book: null
    };

    this.getList = this.getList.bind(this);
    this.bookCallback = this.bookCallback.bind(this);
    this.returnToLib = this.returnToLib.bind(this);
    // this.loan = this.loan.bind(this);
  }

  rowClickHandler(book) {
    this.setState({
      book: book,
      viewBook: true
    })
  }

  returnToLib() {
    this.setState({
      viewBook: false
    });
  }

  componentDidMount(){
    // console.log(this.props);
    this.props.getBooks();
    this.props.getUsers();
  }

  bookCallback() {
    this.setState({
      viewBook: false,
      book: null
    })
  }

  getList(books) {

    if (books) {
      books = books.sort((a, b) => {
        if(a.id < b.id) return -1;
        if(a.id > b.id) return 1;
        return 0;
      });
      const trows =  books.map(book =>
        <tr className="clickable-row" key={book.id} onClick={this.rowClickHandler.bind(this, book)}>
          <td>{book.id}</td>
          <td>{book.name}</td>
          <td>{book.author}</td>
          <td>{book.subject}</td>
          <td>{book.publish_date}</td>
          <td onClick={(e) => {e.stopPropagation()}}>

            {

              <button disabled={!book.available} onClick={(e) => { this.loanBook(e, book)}} data-toggle="modal" data-target="#centralModalSm" className="btn btn-sm btn-secondary">Loan Book</button>
            //   <button onClick={(e) => { this.returnBook(e, book)}} className="btn btn-sm btn-warning">Return</button>
            }

          </td>
        </tr>
      );
      return trows;
    }
  }

  loanBook(e, book) {
    console.log(book);
    this.setState({
      book: book
    })
  }



  render() {
    console.log(this.props.books);
    return (
      <main className="pt-5 mx-lg-5">
        <div className="container-fluid mt-5">

          <div hidden={this.state.viewBook} className="card card-cascade narrower">

              <div className="px-4">

                  <div className="table-wrapper table-responsive">
                      <table className="table table-hover mb-0">

                          <thead>
                              <tr>
                                  <th className="th-lg">
                                      Book ID
                                  </th>
                                  <th className="th-lg">
                                      Book Name
                                  </th>
                                  <th className="th-lg">
                                      Author
                                  </th>
                                  <th className="th-lg">
                                      Subject
                                  </th>
                                  <th className="th-lg">
                                      Published Date
                                  </th>
                                  <th className="th-lg">
                                    ACTIONS
                                  </th>
                              </tr>
                          </thead>

                          <tbody>
                            {
                                this.getList(this.props.books)
                            }
                          </tbody>
                      </table>
                  </div>

              </div>

          </div>
          {
            this.state.viewBook && this.state.book != null ?
            <Book book={this.state.book} bookCallback={this.bookCallback}
                  returnToLib = {this.returnToLib}/> :
            null
          }
        </div>
        <LoanModal book={this.state.book} users={this.props.users} reload={this.props.getBooks}/>
      </main>
    );
  }

  test () {
    console.log(this.props.books)
  }
}

function mapStateToProps(state) {
  return {
      users: state.users,
      books: state.books,
      currentuser: state.currentuser
  }
}

function mapDispatchToProps(dispatch) {
  return {
      getBooks : () => {
          dispatch(getBooks());
      },
      getUsers : () => {
        dispatch(getUsers());
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Library);
