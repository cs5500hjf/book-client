import React, { Component } from "react";
import NoteModal from '../NoteModal/';
import UpdateNoteModal from '../UpdateNoteModal/';
import ModalPrompt from '../ModalPrompt/';

class Notes extends Component {

  constructor(props) {
    super(props);

    this.state = {
      notes: null,
      note: null
    }

    this.fetchNotes = this.fetchNotes.bind(this);
    this.addNote = this.addNote.bind(this);
    this.deleteNote = this.deleteNote.bind(this);
    this.updateNote = this.updateNote.bind(this);
  }

  componentDidMount() {
    // console.log(this.props.book);
    this.fetchNotes();
  }

  fetchNotes() {
    const url = `https://msd-team-1.herokuapp.com/books/${this.props.book.id}/notes`;
    fetch(url)
    .then(res => res.json())
    .then((data) => {
      this.setState({
        notes: data
      });
    })
  }
  
  getList(notes) {
    if (notes && notes.length > 0) {
      notes = notes.sort((a, b) => {
        if(a.id < b.id) return -1;
        if(a.id > b.id) return 1;
        return 0;
      });
      const trows =  notes.map(note => 
        <tr key={note.id} >
          <td>{note.id}</td>
          <td>{note.book_id}</td>
          <td>{note.note}</td>
          <td>
            <button className="btn btn-sm btn-warning z-depth-2" data-toggle="modal" data-target="#updateNoteModal" onClick={() => {this.setState({ note: note})}}>Update</button>
            <button className="btn btn-sm btn-danger z-depth-2" data-toggle="modal" data-target="#promptModalSm" onClick={() => {this.setState({ note: note})}}>Delete</button>
            </td>
        </tr>
      );
      return trows;
    }
  }

  addNote(note, closemodal) {
    const url = `https://msd-team-1.herokuapp.com/books/${this.props.book.id}/notes`;

    fetch(url, {
      method:'POST',
      body: JSON.stringify(note),
      headers: {
          'Content-Type':'application/json'
      }
    })
    .then(res => res.json())
    .then((data) => {
      // alert(data);
      if (data === 'successfully added new note') {
        this.fetchNotes();
        closemodal();
      }
    })
  }

  updateNote(note, closemodal) {
    const url = `https://msd-team-1.herokuapp.com/books/${note.book_id}/notes/${note.id}`;

    fetch(url, {
      method:'PUT',
      body: JSON.stringify(note),
      headers: {
          'Content-Type':'application/json'
      }
    })
    .then(res => res.json())
    .then((data) => {
      // alert(data);
      if (data === 'successfully updated') {
        this.fetchNotes();
        closemodal();
      }
    })
  }

  deleteNote(note) {
    const url = `https://msd-team-1.herokuapp.com/books/${this.props.book.id}/notes/${note.id}`;

    fetch(url, {
      method:'DELETE'
    })
    .then((res) => {return res.json()})
    .then((data) => {
      // alert(data);
      if (data === 'successfully deleted') {
        this.fetchNotes();
      }
    })
  }

  render() {

    return (
      <div className="px-4">
        <br/>
        <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5 text-center"> Notes </h3><hr/><br/>
        <div className="table-wrapper table-responsive">
          <table className="table table-hover mb-0">
            <thead>
              <tr>
                <th className="th-lg">
                    Note ID
                </th>
                <th className="th-lg">
                    Book ID
                </th>
                <th className="th-lg">
                    Notes
                </th>
                <th className="th-lg">
                    Actions
                </th>
              </tr>
            </thead>
            <tbody>
              { 
                  this.getList(this.state.notes)
              }
            </tbody>
          </table>
        </div>
        <div className="text-center mb-4">
          <button className="btn btn-warning z-depth-2" data-toggle="modal" data-target="#addNoteModal">Add Note</button>
          <button className="btn btn-grey z-depth-2" onClick={() => {this.props.goBack()}}>Back</button>
        </div>
        <NoteModal addNote={this.addNote}/>
        <UpdateNoteModal updateNote={this.updateNote} note={this.state.note} />
        <ModalPrompt title="Delete Note" body="Are you sure you want to delete this Note ?" cb={(closeModal) => {this.deleteNote(this.state.note); closeModal();}} />
      </div>
    );
  }
}

export default Notes;
