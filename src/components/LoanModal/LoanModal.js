import React, { Component } from "react";

class LoanModal extends Component {

  constructor(props) {
    super(props);

    this.state ={
      userid: JSON.parse(localStorage.getItem('currentuser')).id,
      error: ''
    }
    this.modalref = React.createRef();
    this.closeModal = this.closeModal.bind(this);
    this.loanBook = this.loanBook.bind(this);
  }


  closeModal() {
    this.modalref.current.click();
  }

  loanBook() {
    if(this.state.userid && this.state.userid !== '' && this.state.userid !== '--Select--') {
      let url = `https://msd-team-1.herokuapp.com/users/${this.state.userid}/books/${this.props.book.id}`;
      fetch(url, {
        method:'POST'
      })
      .then(res => res.json())
      .then((data) => {
        // alert(data);
        if (data === 'successfully borrowed the book'){
          this.props.reload();
          this.closeModal();
        }
      });
    }
    else {
      this.setState({
        error: 'Select User'
      })
    }
  }

  render() {
    return (
      <div className="modal fade" id="centralModalSm" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title w-100" id="myModalLabel">Loan Book</h4>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" ref={this.modalref}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p> Are you sure you want to Borrow this Book ?</p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary btn-sm" onClick={this.loanBook}>Loan Book</button>
            </div>
          </div>
        </div>
      </div>);
  }
}

export default LoanModal;
