import React, { Component } from "react";

class NavBar extends Component {
  render() {
    return (
      <nav className="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
          <div className="container-fluid">

              
              <a className="navbar-brand waves-effect" href="/" >
                  <strong className="red-text">BOOK PROJECT</strong>
              </a>

              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                  aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
              </button>

              
              <div className="collapse navbar-collapse" id="navbarSupportedContent">

                  
                  <ul className="navbar-nav mr-auto">
                      <li className="nav-item active">
                          <a className="nav-link waves-effect" >Home
                              <span className="sr-only">(current)</span>
                          </a>
                      </li>
                  </ul>

                  
                  <ul className="navbar-nav nav-flex-icons">
                      <li className="nav-item">
                          <a href="https://bitbucket.org/cs5500hjf/book-client/src/master/" className="nav-link border border-light rounded waves-effect"
                              target="_blank" rel="noopener noreferrer">
                              <i className="fa fa-bitbucket mr-2"></i> Bitbucket
                          </a>
                      </li>
                  </ul>

              </div>

          </div>
      </nav>
    );
  }
}

export default NavBar;
