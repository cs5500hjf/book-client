import React, { Component } from "react";
import AddBookToCollectionModal from '../AddBookToCollectionModal/';
import  FavoritesPrompt  from '../FavoritesPrompt/';
import { getBooks, getCollection } from "../../actions";

import { connect } from 'react-redux';

class Favorites extends Component {

  constructor(props) {
    super(props);

    this.state = {
      favbooks : null,
      collection_name: null
    }

    this.getFavoriteBooks = this.getFavoriteBooks.bind(this);
    this.deleteCollection = this.deleteCollection.bind(this);
    this.removeBookFromCollection = this.removeBookFromCollection.bind(this);
    this.reloadList = this.reloadList.bind(this);

  }

  removeBookFromCollection(bookid){
    const currentuser = JSON.parse(localStorage.getItem('currentuser'));

    let url = `https://msd-team-1.herokuapp.com/users/${currentuser.id}/collections/${this.state.collection_name}/books/${bookid}`;

    fetch(url, {
      method:'DELETE'
    })
      .then((res) => {return res.json()})
      .then((data) => {
        if (data === 'successfully delete collection') {
          this.getFavoriteBooks(this.state.collection_name);
        }
      })

  }

  deleteCollection() {
    const currentuser = JSON.parse(localStorage.getItem('currentuser'));

    const url = `https://msd-team-1.herokuapp.com/users/${currentuser.id}/collections`;

    let info = {};
    info.collection_name = this.state.collection_name;

    fetch(url, {
      method:'DELETE',
      body: JSON.stringify(info),
      headers: {
        'Content-Type':'application/json'
      }
    })
      .then((res) => {return res.json()})
      .then((data) => {
        if (data === 'successfully delete collection') {
          this.props.getCollection();
          this.getFavoriteBooks(this.state.collection_name);
          this.props.history.push('/home/library');
          // this.getFavBooksList(this.state.favbooks);

        }
      })
  }


  reloadList() {
    this.getFavoriteBooks(this.state.collection_name);
  }

  getFavBooksList(books) {
    console.log(this.props);
    if (books) {
      books = books.sort((a, b) => {
        if(a.id < b.id) return -1;
        if(a.id > b.id) return 1;
        return 0;
      });
      const trows =  books.map(book =>
        <tr className="clickable-row" key={book.id}>
          <td>{book.id}</td>
          <td>{book.name}</td>
          <td>{book.author}</td>
          <td>{book.subject}</td>
          <td>{book.publish_date}</td>
          <td>
              <button className="btn btn-sm btn-danger" onClick= {() => {this.removeBookFromCollection(book.id)}}>Remove Book</button>
          </td>
        </tr>
      );
      return trows;
    }
  }

  componentWillReceiveProps(nextProps){

    if(nextProps.match.params.collection_name !== this.props.match.params.collection_name){
      // console.log(nextProps.match.params.collection_name);
      // console.log(this.props.match.params.collection_name);
      this.getFavoriteBooks(nextProps.match.params.collection_name);
    }

  }

  getFavoriteBooks(collection_name){
    const currentuser = JSON.parse(localStorage.getItem('currentuser'));

    let url = `https://msd-team-1.herokuapp.com/users/${currentuser.id}/collections/${collection_name}`;
    fetch(url)
      .then(res => res.json())
      .then(books => {
        // console.log(books);
        this.setState(
          {favbooks : books,
           collection_name: collection_name});
      });

  }

  componentDidMount(){
    // console.log(this.props);
    let collection_name = this.props.match.params.collection_name;
    // console.log(collection_name);
    // this.setState({collection_name: collection_name});
    this.props.getBooks();
    this.getFavoriteBooks(collection_name);
  }



  render() {

    return (
      <main className="pt-5 mx-lg-5">
        <div className="container">
          <div className="row mt-5 pt-3 justify-content-center">
            <div className="card card-cascade narrower col-lg-11 col-11 mt-1 mx-lg-4">
              <div className="px-4">
                <br/>
                <h3 className="deep-grey-text mt-3 mb-4 pb-1 text-center"> Favorite Books </h3>
                <div className="row px-2">
                </div><hr/>
                <div className="table-wrapper table-responsive">
                  <table className="table table-hover mb-0">
                    <thead>
                    <tr>
                      <th className="th-lg">
                        Book ID
                      </th>
                      <th className="th-lg">
                        Book Name
                      </th>
                      <th className="th-lg">
                        Author
                      </th>
                      <th className="th-lg">
                        Subject
                      </th>
                      <th className="th-lg">
                        Published Date
                      </th>
                      <th className="th-lg">
                        ACTION
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      this.getFavBooksList(this.state.favbooks)
                    }
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="text-center mb-4">
                <button className="btn btn-warning z-depth-2" data-toggle="modal" data-target="#addBookToCollec">Add Book</button>
                <button className="btn btn-danger z-depth-2" data-toggle="modal" data-target="#favpromptModalSm">Delete Collection</button>
              </div>
            </div>
          </div>
        </div>
        <AddBookToCollectionModal collection_name={this.state.collection_name} reloadList={this.reloadList} books={this.props.books} favbooks={this.state.favbooks} />
        <FavoritesPrompt title="Delete Collection" body="Are you sure you want to delete this collection ?" cb={(closeFavModal) => {this.deleteCollection(); closeFavModal();}} />
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {
    books: state.books,
    favbooks: state.favbooks
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getBooks : () => {
      dispatch(getBooks());
    },
    getCollection : () => {
      dispatch(getCollection());
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
