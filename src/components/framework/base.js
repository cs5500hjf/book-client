import React, { Component } from "react";
import { Route, Switch, Redirect} from 'react-router-dom';

import SideBar from '../SideBar/';
import Library from "../Library";
import BookManage from '../BookManage/';
import Favorites from '../Favorites/';
import User from '../User/';
import BorrowedBooks from '../BorrowedBooks/';
import Profile from '../Profile/';
import FavoritesManage from '../FavoritesManage/';
import Notes from '../Notes/';
import SearchBook from '../SearchBook/';

class base extends Component {
    render() {
        // console.log(this.props);

        return (
            <div>

                <SideBar />

                <Switch>
                    <Route path='/home/library' component={Library} />
                    <Route path='/home/bookmanage' component={BookManage} />
                    <Route path='/home/notes/:bookid' component={Notes} />
                    <Route path='/home/collection/new' component={FavoritesManage} />
                    <Route path='/home/collections/:collection_name' component={Favorites} />
                    <Route path='/home/borrowed' component={BorrowedBooks} />
                    <Route path='/home/search' component={SearchBook} />
                    <Route path='/home/user' component={User} />
                    <Route path='/home/profile' component={Profile} />
                    <Redirect to='/home/library' />
                </Switch>

            </div>
        );
    }
}

export default base;
