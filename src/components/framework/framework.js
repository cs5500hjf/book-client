import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import Landing from '../Landing';
import base from './base';
import NavBar from '../NavBar/';


class framework extends Component {

  render() {
    return (

      <BrowserRouter>
        <div>
          <NavBar />

          <Switch>
            <Route path='/home' name='base' component={base} />
            <Route path="/" name="Landing" component={Landing} />
            <Redirect to='/login' />
          </Switch>
        </div>
      </BrowserRouter>

    );
  }
}

export default framework;
