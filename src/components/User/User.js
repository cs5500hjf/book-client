import React, { Component } from "react";
import { connect } from 'react-redux';
import { getUsers } from '../../actions/index';
import ModalPrompt  from '../ModalPrompt/';

class User extends Component {

  getList(users) {
    if (users) {
      const trows =  users.map(user => 
        <tr key={user.id} >
          <td>{user.id}</td>
          <td>{user.name}</td>
          <td>{user.user_name}</td>
          <td>{user.email}</td>
        </tr>
      );
      return trows;
    }
  }

  componentDidMount(){
    this.props.getUsers();
  }

  remindUsers(closeModal) {
    let url = `https://msd-team-1.herokuapp.com/remind`;
    fetch(url)
    .then(res => res.json())
    .then((data) => {
      if (data.message) {
        closeModal();
        alert(data.remind_list);
      }
    })
  }
  
  render() {
    return (
      <main className="pt-5 mx-lg-5">
        <div className="container">
          <div className="row mt-5 pt-3 justify-content-center">
            <div className="card card-cascade narrower col-lg-11 col-11 mt-1 mx-lg-4">
              <div className="px-4">
                <br/>
                <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5 text-center"> USERS </h3><hr/><br/>
                <div className="table-wrapper table-responsive">
                  <table className="table table-hover mb-0">
                    <thead>
                      <tr>
                        <th className="th-lg">
                            User ID
                        </th>
                        <th className="th-lg">
                            Name
                        </th>
                        <th className="th-lg">
                            Username
                        </th>
                        <th className="th-lg">
                            Email
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      { 
                          this.getList(this.props.users)
                      }
                    </tbody>
                  </table>
                </div>
                <br />
                <div className="text-center mb-4">
                    <button className="btn btn-danger" data-toggle="modal" data-target="#promptModalSm" >Remind Users</button>
                </div>
                <ModalPrompt title="Remind all Users" body="Are you sure you want to send email remainder to all users?" cb={(closeModal) => { this.remindUsers(closeModal) }} />
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
  
}

function mapStateToProps(state) {
  return {
      users: state.users
  }
}

function mapDispatchToProps(dispatch) {
  return {
      getUsers : () => {
          dispatch(getUsers());
      }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(User);