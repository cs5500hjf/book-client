import React, { Component } from "react";

class NoteModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      note: null,
      error: ''
    }

    this.modalref = React.createRef();
    this.closeModal = this.closeModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    // console.log('Val:' + value + ' ' + name);
    this.setState({
      [name]: value
    });
    // console.log(this.state);
  }

  closeModal() {
    this.modalref.current.click();
  }

  submitNote() {
    if (this.state.note && this.state.note !== null && this.state.note !== '') {
      this.props.addNote({ note: this.state.note }, this.closeModal)
    } else {
      this.setState({
        error: 'Enter a valid note'
      })
    }
    
  }

  render() {
    return (
      <div className="modal fade" id="addNoteModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div className="modal-dialog modal-lg" role="document">

          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title w-100" id="myModalLabel">Add Note</h4>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close" ref={this.modalref}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <section className="form-simple ">
                <div className="md-form pb-3">
                    <textarea id="Form-note" name="note" className="md-textarea form-control" onChange={this.handleChange}/>
                    <label className="active" htmlFor="Form-note">Note</label>
                    <div className="font-small red-text d-flex justify-content-start">{this.state.error}</div>
                </div>
              </section>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
              <button type="button" disabled={!this.state.note || this.state.note === null} className="btn btn-primary btn-sm" onClick={() => {this.submitNote()}}>Submit</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NoteModal;
