import React, { Component } from "react";

class BorrowedBooks extends Component {

  constructor(props) {
    super(props);
    this.state = {
      books: null
    }

    this.getList = this.getList.bind(this);
    this.fetchBook = this.fetchBook.bind(this);
  }

  fetchBook() {
    const user = JSON.parse(localStorage.getItem('currentuser'));
    console.log(user);
    let url = `https://msd-team-1.herokuapp.com/users/${user.id}/books`;
    fetch(url, {
      method:'GET'
    })
    .then(res => res.json()) 
    .then((data) => {
      console.log(data);
      this.setState({
        books: data
      })
    });
  }

  componentDidMount() {
    this.fetchBook();
  }

  getList(books) {
    if (books) {
      const trows = books.map(book => 
        <tr className="clickable-row" key={book.borrow_record.id + book.borrow_record.return_date} >
          <td>{book.book_detail.id}</td>
          <td>{book.book_detail.name}</td>
          <td>{book.borrow_record.borrow_date}</td>
          <td>{book.borrow_record.return_date}</td>
          
          <td>{book.borrow_record.overdue ? 'True' : 'False'}</td>
          <td onClick={(e) => {e.stopPropagation()}}> 
            {
              book.borrow_record.is_returned ? null : <button className="btn btn-sm btn-secondary" onClick={() => {this.returnBook(book)}}>Return Book</button>
            }
          </td>
        </tr>
      );
      return trows;
    }
  }

  returnBook(book) {
    let url = `https://msd-team-1.herokuapp.com/users/${book.borrow_record.user_id}/books/${book.book_detail.id}`;
    fetch(url, {
      method:'PUT'
    })
    .then(res => res.text())
    .then((data) => {
      alert(data);
      if (data.startsWith('successfully return the book')) {
       this.fetchBook(); 
      }
    });
  }

  render() {
    return (
      <main className="pt-5 mx-lg-5">
        <div className="container-fluid mt-5">
          
          <div className="card card-cascade narrower justify-content-center">

              <div className="px-4">

                  <div className="table-wrapper table-responsive">
                      {
                        this.state.books && this.state.books.length > 0 ?
                        <table className="table table-hover mb-0">

                            <thead>
                                <tr>
                                    <th className="th-lg">
                                        Book ID
                                    </th>
                                    <th className="th-lg">
                                        Book Name
                                    </th>
                                    <th className="th-lg">
                                        Borrowed Date
                                    </th>
                                    <th className="th-lg">
                                        Rerturn Date
                                    </th>
                                    
                                    <th className="th-lg">
                                        Overdue
                                    </th>
                                    <th className="th-lg">
                                        Actions
                                    </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                              { 
                                  this.getList(this.state.books)
                              }
                            </tbody>
                        </table> : 
                         <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5 text-center"> No Borrowed books </h3>
                      }
                      
                  </div>
              </div>
          </div>
        </div>
      </main>
    );
  }
}

export default BorrowedBooks;
