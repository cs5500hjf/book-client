import React, { Component } from "react";
import { Route } from 'react-router-dom';

// import SideBar from '../SideBar/';
import Library from '../Library/';
import BookManage from '../BookManage/';


class Home extends Component {
  render() {
    return (
      <div>
        {/* <SideBar /> */}
        <Route path="/home/library" name="Library" component={Library} />
        <Route path="/home/bookmanage" name="BookManage" component={BookManage} />
        {/* <Library /> */}
      </div>
    );
  }
}

export default Home;
