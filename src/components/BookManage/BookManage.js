import React, { Component } from "react";
// import { Button, Panel, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';

// function FieldGroup({ id, label, help, ...props }) {
//     return (
//         <FormGroup controlId={id}>
//             <ControlLabel>{label}</ControlLabel>
//             <FormControl {...props} />
//             {help && <HelpBlock>{help}</HelpBlock>}
//         </FormGroup>
//     );
// }

class BookManage extends Component {
    constructor() {
        super();
        this.state = {
            value: {
                "bookname": '',
                "author": '',
                "subject": '',
                "publishdate": ''
            }
        }
        this.handleChange = this.handleChange.bind(this);
        this.addToLib = this.addToLib.bind(this);
    }

    handleChange(e) {
        var newVal = this.state.value;
        newVal[e.target.id] = e.target.value;
        this.setState({ value: newVal });
    }

    addToLib() {
        let url = `https://msd-team-1.herokuapp.com/books`;

        var info = {};
        info.name = this.state.value.bookname;
        info.author = this.state.value.author;
        info.subject = this.state.value.subject;
        info.publish_date = this.state.value.publishdate;


        fetch(url, {
            method:'POST',
            body: JSON.stringify(info),
            headers: {
                'Content-Type':'application/json'
            }
        })
        .then(res => res.json())
        .then((json) => {
            var data = json;
            console.log(data);
            if(data.id) {
                alert('Added Book successfully. BookID:' + data.id);
                const { history } = this.props;
                history.push('/home');
            } else {
                alert('Unable to add book. Something went wrong');
            }
        })
    }

    render() {
        return (
            <main className="pt-5 mx-lg-5">
                <div className="container-fluid mt-5">
                  <div className="row mt-5 pt-3 justify-content-center">
                    <div className="card card-cascade narrower  col-lg-7 col-7 mt-1 mx-lg-4">
                        <div className="px-4">
                            <br/>
                            <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5 text-center"> ADD NEW BOOK </h3><hr/><br/>

                             <section className="form-simple ">
                                <div className="md-form pb-3">
                                    <input type="text" id="bookname"  name="bookname" className="form-control" onChange={this.handleChange} />
                                    <label className="active" htmlFor="bookname">Book Name</label>
                                </div>

                                <div className="md-form pb-3">
                                    <input type="text" id="author"  name="author" className="form-control" onChange={this.handleChange} />
                                    <label className="active" htmlFor="author">Author Name</label>
                                </div>
                                
                                <div className="md-form pb-3">
                                    <input type="text" id="subject"  name="subject" className="form-control" onChange={this.handleChange} />
                                    <label className="active" htmlFor="subject">Subject</label>
                                </div>

                                <div className="md-form pb-3">
                                    <input type="date" id="publishdate"  name="publishdate" className="form-control" onChange={this.handleChange} />
                                    <label className="active" htmlFor="publishdate">Publish Date</label>
                                </div>

                                <div className="text-center mb-4">
                                    <button type="button" onClick={this.addToLib} className="btn btn-primary z-depth-2">Add </button>
                                </div>
                            </section>
                            {/*
                                <Panel>
                                <Panel.Heading>
                                    <Panel.Title componentClass="h3">Add a new book</Panel.Title>
                                </Panel.Heading>
                                <Panel.Body>
                                    <FieldGroup
                                        id="bookname"
                                        type="text"
                                        label="Book Name"
                                        placeholder="Enter book name"
                                        value={this.state.value["bookname"]}
                                        onChange={this.handleChange}
                                    />
                                    <FieldGroup
                                        id="author"
                                        type="text"
                                        label="Author Name"
                                        placeholder="Enter author name"
                                        value={this.state.value["author"]}
                                        onChange={this.handleChange}
                                    />
                                    <FieldGroup
                                        id="subject"
                                        type="text"
                                        label="Subject"
                                        placeholder="Enter genre"
                                        value={this.state.value["subject"]}
                                        onChange={this.handleChange}
                                    />
                                    <FieldGroup
                                        id="publishdate"
                                        type="date"
                                        label="Publish Date"
                                        placeholder="Enter publish date"
                                        value={this.state.value["publishdate"]}
                                        onChange={this.handleChange}
                                    />
                                    <Button bsStyle="success" onClick={this.addToLib}>Add</Button>
                                </Panel.Body>
                            </Panel>
                         */}
                        </div>
                    </div>
                  </div>
                </div>
            </main>
        );
    }

}

export default BookManage;
