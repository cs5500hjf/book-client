import React, { Component } from "react";
import { NavLink, Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { resetCurrentUser, getCollection } from '../../actions/index';
import { connect } from 'react-redux';

class SideBar extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      collapse: true,
      collections: null
    }
    this.linkActive = this.linkActive.bind(this);
    this.getCollections = this.getCollections.bind(this);
    this.reloadCollections = this.reloadCollections.bind(this);
  }

  componentDidMount(){
    this.props.getCollection();
  }



  getCollectionsList(){

    const collection_list = this.props.collections;

    if(collection_list){

     let collection_sidebar = collection_list.map(collection =>
        <NavLink hidden={this.state.collapse} key={collection} className="list-group-item list-group-item-action waves-effect" to={'/home/collections/'+ collection} activeClassName="">
          <i className="mr-5"></i>{collection}</NavLink>);

     return collection_sidebar;
    }
  }

  getCollections(){
    const currentuser = JSON.parse(localStorage.getItem('currentuser'));


    let url = `https://msd-team-1.herokuapp.com/users/${currentuser.id}/collections`;
    fetch(url)
      .then(res => res.json())
      .then(collections => {
        console.log(collections);
        this.setState({collections : collections});
      });
  }

  reloadCollections() {
    this.getCollections();
    // this.getCollectionsList();
  }

  render() {
    // Replace this with fetch call
    //
    //*************************************** */
    return (
      <div className="sidebar-fixed position-fixed">

        <Link className="logo-wrapper waves-effect" to="/">

        </Link>

        <div className="list-group list-group-flush">
          <NavLink className="list-group-item list-group-item-action waves-effect" to="/home/library" activeClassName="active">
            <i className="fa fa-book mr-3" ></i>Library</NavLink>
          <NavLink className="list-group-item list-group-item-action waves-effect" to="/home/bookmanage" activeClassName="active">
            <i className="fa fa-plus mr-3"></i>Add New Book </NavLink>
          <NavLink className="list-group-item list-group-item-action waves-effect" to="/home/collection/new" activeClassName="active">
            <i className="fa fa-plus mr-3"></i>Add New Collection </NavLink>

          <a className={!this.linkActive() ? "list-group-item list-group-item-action waves-effect" : "list-group-item list-group-item-action waves-effect active" }  onClick={() => {this.setState({collapse: !this.state.collapse}); this.getCollections();}} >
            <i className="fa fa-star mr-3"></i>Collections &nbsp; <i className={this.state.collapse ? "fa fa-angle-down" : "fa fa-angle-up"}/>
            </a>
          {
            this.getCollectionsList()
          }
          <NavLink className="list-group-item list-group-item-action waves-effect" to="/home/borrowed" activeClassName="active">
            <i className="fa fa-tasks mr-3"></i>Borrowed Book</NavLink>
          <NavLink className="list-group-item list-group-item-action waves-effect" to="/home/search" activeClassName="active">
            <i className="fa fa-search mr-3"></i>Search Book</NavLink>
          <NavLink className="list-group-item list-group-item-action waves-effect" to="/home/user" activeClassName="active">
            <i className="fa fa-users mr-3"></i>Users</NavLink>
          <NavLink className="list-group-item list-group-item-action waves-effect" to="/home/profile" activeClassName="active">
            <i className="fa fa-user-circle-o mr-3"></i>Profile</NavLink>
          <Link className="list-group-item list-group-item-action waves-effect" onClick={ () => { localStorage.removeItem('currentuser'); this.props.resetCurrentUser() }} to="/">
            <i className="fa fa-sign-out mr-3"></i>Logout</Link>

        </div>

      </div>
    );
  }

  linkActive() {
    const path = this.props.location.pathname;
    return path.startsWith('/home/collections/');
  }
}

function mapStateToProps(state) {
  return {
      currentuser: state.currentuser,
      collections: state.collections
  }
}

function mapDispatchToProps(dispatch) {
  return {
      resetCurrentUser: () => {
          dispatch(resetCurrentUser())
      },
      getCollection : () => {
        dispatch(getCollection());
      }

  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps, null, {pure: false})(SideBar));
