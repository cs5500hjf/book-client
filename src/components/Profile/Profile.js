import React, { Component } from "react";
import { connect } from 'react-redux';
import { resetCurrentUser } from '../../actions/index';

class Profile extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      editing: false, 
      name: null,
      password: null,
      confirm: null,
      error: ''
    };

    this.deleteUser = this.deleteUser.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  
  render() {
    
    if (this.props.currentuser) {
      return this.renderUser(this.props.currentuser);
    } else {
      var currentuser = JSON.parse(localStorage.getItem('currentuser'));
      // console.log(currentuser);
      if (currentuser) {
        return this.renderUser(currentuser)
      }
      return <div />;
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    // console.log('Val:' + value + ' ' + name);
    this.setState({
      [name]: value
    });
  }

  renderUser(user) {

    return (
      <main className="pt-5 mx-lg-5">
        <div className="container">
          <div className="row mt-5 pt-3 justify-content-center">
            <div className="card card-cascade narrower col-lg-9 col-9 mt-1 mx-lg-4">
  
              <div className="px-4">
                <br/>
                <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5 text-center"> PROFILE </h3><hr/><br/>
                <section className="form-simple ">
  
                  <div className="md-form pb-3">
                      <input type="text" id="Form-userid" disabled name="id" className="form-control" value={user.id} />
                      <label className="active" htmlFor="Form-userid">User Id</label>
                  </div>
  
                  <div className="md-form pb-3">
                      <input type="text" id="Form-username"  disabled name="username" className="form-control" value={user.user_name} />
                      <label className="active" htmlFor="Form-username">Username</label>
                  </div>

                  <div className="md-form pb-3">
                      <input type="text" id="Form-email"  disabled required name="email" className="form-control" defaultValue={user.email} />
                      <label className="active" htmlFor="Form-email">Email</label>
                  </div>
  
                  <div className="md-form pb-3">
                      <input type="text" id="Form-name"  disabled={!this.state.editing} name="name" required className="form-control" defaultValue={user.name} onChange={this.handleChange} />
                      <label className="active" htmlFor="Form-name">Name</label>
                      <div className="font-small red-text d-flex justify-content-start">{this.state.error}</div>
                  </div>

                  {
                    this.state.editing ? 
                    <div className="md-form pb-3">
                      <input type="text" id="Form-password"  required name="password" className="form-control"  onChange={this.handleChange} />
                      <label  htmlFor="Form-password">New Password</label>
                    </div> : null
                  }
                  
                  {
                    this.state.editing ? 
                    <div className="md-form pb-3">
                      <input type="text" id="Form-confirm" required name="confirm" className="form-control" onChange={this.handleChange} />
                      <label  htmlFor="Form-confirm">Confirm New Password</label>
                    </div> : null
                  }
                  
  
                  <div className="text-center mb-4">
                    { this.state.editing ? 
                      <div><button type="button"  className="btn btn-warning z-depth-2" onClick={() => {this.updateUser(user)}}>Save</button>
                      <button type="button" className="btn btn-grey z-depth-2" onClick={() => { this.setState({ editing: false, error: ''})}}>Cancel</button></div> :
                      <div><button type="button" className="btn btn-warning z-depth-2" onClick={() => { this.setState({ editing: true})}}>Update</button>
                      <button type="button" className="btn btn-danger z-depth-2" onClick={() => {this.deleteUser(user)}}>Delete</button>
                      </div>
                    }
                      
                    
                  </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </main>
      );
  }

  deleteUser(user) {
    if (window.confirm('Are you sure you wish to delete your Account?')) {
      console.log('Delete'); 
      let url = `https://msd-team-1.herokuapp.com/users/${user.id}`;

      fetch(url, {
        method:'DELETE'
      })
      .then(res => res.json())
      .then((data) => {
        if (data === 'successfully deleted') {
          localStorage.removeItem('currentuser'); 
          this.props.resetCurrentUser();
          const { history } = this.props;
          history.push('/');
        } else {
          this.setState({
            error: 'Delete Failed. Something went wrong'
          })
        }
      })
    } 
  }

  updateUser(user) {
    const name = this.state.name;
    const password = this.state.password;
    const confirm = this.state.confirm;

    if (name === '' || password === '' || confirm === '') {
      this.setState({
        error: 'Name & Password are required fields'
      })
    } else {

      if ((password && confirm && password !== confirm) || password === null) {
        this.setState({
          error: "Passwords don't match"
        })
      } else {
        user.password = password;
        user.name = name;

        // console.log(user);

        let url = `https://msd-team-1.herokuapp.com/users/${user.id}`;

        fetch(url, {
              method:'PUT',
              body: JSON.stringify(user),
              headers: {
                  'Content-Type':'application/json'
              }
          })
          .then(res => res.json())
          .then((data) => {
              // console.log(data);
              if (data === 'successfully updated') {
                localStorage.setItem('currentuser', JSON.stringify(user));
             
                this.setState({
                  error: '',
                  editing: false
                });
              } else {
                this.setState({
                  error: 'Update Failed. Something went wrong'
                })
              }
        });
      }
    }
  }
}

function mapStateToProps(state) {
  return {
      books: state.books,
      currentuser: state.currentuser
  }
}

function mapDispatchToProps(dispatch) {
  return {
      resetCurrentUser: () => {
          // console.log('in map: ' + user.email);
          dispatch(resetCurrentUser())
      }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
