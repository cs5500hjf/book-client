import React, { Component } from "react";
import { connect } from 'react-redux';
import { getUsers, setCurrentUser } from '../../actions/index';
import { withRouter } from 'react-router-dom';

class Login extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            username: '',
            password: '',
            error: ''
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.login = this.login.bind(this);
    }

    componentDidMount(){
        // console.log(this.props);
        this.props.getUsers();
    }

  login() {
    const username = this.state.username;
    const password = this.state.password;
    console.log(this.props.users);
    const users = this.props.users;
    if (users) {
        users.map(user => {
            if (user.user_name === username && user.password === password) {
              this.props.setCurrentUser(user);
              localStorage.setItem('currentuser', JSON.stringify(user));
              const { history } = this.props;
              history.push('/home');
              console.log('hello');
              return true;
            }
            
            return false;
        });
        this.setState ({
            error: "Invalid credentails"
        })
    } else {
        this.setState ({
            error: "Make sure you are connected to the internet"
        })
    }
    
     
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    // console.log('Val:' + value + ' ' + name);
    this.setState({
      [name]: value
    });
  }



  render() {
    return (
      <section>
        <div className="container-fluid ">
            <hr className="my-5"/>
            <div className="container">
                <div className="row mt-5 pt-3 justify-content-center">

                    <div className="col-lg-6 col-6 mt-1 mx-lg-4">

                        <section className="section extra-margins pb-3 text-center text-lg-left">

                            <div className="row mb-5 pb-3">

                                <div className="col-md-12">
                                <section className="form-simple">

                                  <div className="card">

                                      <div className="header pt-3 grey lighten-2">

                                          <div className="row d-flex justify-content-start">
                                              <h3 className="deep-grey-text mt-3 mb-4 pb-1 mx-5">Log in</h3>
                                          </div>

                                      </div>

                                      <div className="card-body mx-4 mt-4">

                                          <div className="md-form">
                                              <input type="text" id="Form-email4" name="username" className="form-control"  onChange={this.handleChange} />
                                              <label htmlFor="Form-email4">Username</label>
                                          </div>

                                          <div className="md-form pb-3">
                                              <input type="password" id="Form-pass4" name="password" className="form-control"  onChange={this.handleChange}/>
                                              <label htmlFor="Form-pass4">Password</label>
                                              <p className="font-small grey-text d-flex justify-content-end">Forgot <a   className="dark-grey-text font-weight-bold ml-1"> Password?</a></p>
                                              <div className="font-small red-text d-flex justify-content-start">{this.state.error}</div>
                                          </div>
                                         

                                          <div className="text-center mb-4">
                                              <button type="button" className="btn btn-danger btn-block z-depth-2" onClick={this.login}>Log in</button>
                                          </div>
                                          <p className="font-small grey-text d-flex justify-content-center">Don't have an account? <a className="dark-grey-text font-weight-bold ml-1" onClick={this.props.toggleRegister}> Register</a></p>

                                      </div>

                                  </div>

                                  </section>

                                </div>

                            </div>

                          </section>

                      </div>

                  </div>

              </div>

          </div>

        </section>
          );
  }
}

function mapStateToProps(state) {
    // console.log(state);
    return {
        users: state.users,
        currentuser: state.currentuser
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getUsers : () => {
            dispatch(getUsers());
        },
        setCurrentUser: (user) => {
            // console.log('in map: ' + user.email);
            dispatch(setCurrentUser(user))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
